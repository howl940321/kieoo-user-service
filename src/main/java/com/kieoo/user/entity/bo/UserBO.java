package com.kieoo.user.entity.bo;

import lombok.Data;

/**
 * 拼接用户的多个表,封装统一的用户信息对象
 */
@Data
public class UserBO {
    /**
     * 用户id
     */
    private long userId;
    /**
     * 用户账号
     */
    private String account;
    /**
     * 密码
     */
    private String password;
    /**
     * 用户昵称
     */
    private String nickName;
    /**
     * 用户性别
     */
    private int sex;
    /**
     * 手机号码
     */
    private String phoneNumber;
    /**
     * 出生日期
     */
    private String birthDate;
    /**
     * 个性签名
     */
    private String signature;
    /**
     * 头像
     */
    private String headImg;
}
