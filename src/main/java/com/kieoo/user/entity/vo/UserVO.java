package com.kieoo.user.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * VO用于承载返回给前端的对象
 */
@Data
public class UserVO {
    /**
     * jwt token
     */
    private String jwtToken;
    /**
     * 用户id,如果不加这个注解,userId会从980465985028882433变成980465985028882400
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private long userId;
    /**
     * 用户账号
     */
    private String account;
    /**
     * 用户昵称
     */
    private String nickName;
    /**
     * 用户性别
     */
    private int sex;
    /**
     * 手机号码
     */
    private String phoneNumber;
    /**
     * 出生日期
     */
    private String birthDate;
    /**
     * 个性签名
     */
    private String signature;
    /**
     * 头像
     */
    private String headImg;
}
