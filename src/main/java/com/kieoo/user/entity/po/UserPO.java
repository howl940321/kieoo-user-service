package com.kieoo.user.entity.po;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 与数据库中user表对应
 *
 * @author howl-xu
 * @since 2024-03-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserPO implements Serializable {
    /**
     * 用户id
     */
    private long userId;
    /**
     * 用户账号
     */
    private String account;
    /**
     * 用户昵称
     */
    private String nickName;
    /**
     * 用户性别
     */
    private int sex;
    /**
     * 手机号码
     */
    private String phoneNumber;
    /**
     * 出生日期
     */
    private String birthDate;
    /**
     * 个性签名
     */
    private String signature;
    /**
     * 头像
     */
    private String headImg;

}
