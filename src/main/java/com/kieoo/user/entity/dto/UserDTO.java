package com.kieoo.user.entity.dto;

import lombok.Data;

import javax.validation.constraints.*;


/**
 * DTO用于承接前端传入的对象
 */
@Data
public class UserDTO {
    /**
     * 用户id
     */
    private String userId;
    /**
     * 用户账户,6到16位,需要同时包含小写字母及数字
     */
    @Pattern(regexp = "^(?=.*[a-z])(?=.*\\d)[a-z\\d]{6,16}$", message = "账号需包含小写字母及数字，6到16位长度")
    private String account;
    /**
     * 密码（使用rsa公钥加密）
     */
    private String passwordEncrypted;

    private String password;
    /**
     * 用户是否勾选记住我
     */
    private String rememberMe;

    /**
     * 行为验证码数据,用于登录校验
     */
    private String captchaVerification;

    /**
     * 用户昵称
     */
    @Pattern(regexp = "^[\\u4E00-\\u9FA5A-Za-z0-9]{1,8}$", message = "仅支持字母，数字，中文，长度小于8字符")
    private String nickName;

    /**
     * 用户性别,整型值不支持Pattern模式,仅字符串支持
     */
    @Min(value = 0, message = "请选择用户性别")
    @Max(value = 1, message = "请选择用户性别")
    private int sex;

    /**
     * 手机号码
     */
    @Pattern(regexp = "^1[3-9]\\d{9}$", message = "请输入正确的手机号")
    private String phoneNumber;

    /**
     * 个性签名
     */
    @Pattern(regexp = "^[\\u4E00-\\u9FA5A-Za-z0-9，。！……￥@@#$%^&*()]{1,100}$", message = "仅支持字母，数字，中文，部分特殊字符，长度小于100字符")
    private String signature;

    /**
     * 出生日期
     */
    private String birthDate;
}
