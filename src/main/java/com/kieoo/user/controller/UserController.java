package com.kieoo.user.controller;

import com.kieoo.user.common.KieooCommonResponse;
import com.kieoo.user.entity.bo.UserBO;
import com.kieoo.user.entity.dto.UserDTO;
import com.kieoo.user.entity.po.UserPO;
import com.kieoo.user.entity.vo.UserVO;
import com.kieoo.user.service.UserService;
import com.kieoo.user.utils.RegexUtils;
import com.kieoo.user.utils.RsaUtils;
import com.kieoo.user.utils.Sha512Utils;
import com.kieoo.user.utils.UserUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author howl-xu
 * @version 1.0
 * Create by 2024/3/27
 */
@Slf4j
@RestController
@RequestMapping("/user/")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 更新用户信息
     *
     * @param userDTO userDTO
     */
    @PostMapping("/update/v1")
    public KieooCommonResponse update(@RequestBody UserDTO userDTO) {
        UserBO userBO = new UserBO();
        userBO.setUserId(Long.valueOf(userDTO.getUserId()));
        userBO.setNickName(userDTO.getNickName());
        userBO.setSignature(userDTO.getSignature());
        userBO.setBirthDate(userDTO.getBirthDate());
        userService.updateUser(userBO);
        return KieooCommonResponse.builder().code(0).message("user update success").data("").build();
    }

    /**
     * 更新密码
     * @param oldPasswordEncrypted 旧密码
     * @param newPasswordEncrypted 新密码
     * @return 返回结果
     * @throws Exception exception
     */
    @PostMapping("/update-password/v1")
    public KieooCommonResponse updatePassword(@RequestParam String oldPasswordEncrypted, @RequestParam String newPasswordEncrypted) throws Exception{
        // 明文密码
        String oldPassword = RsaUtils.decryptByPrivateKey(oldPasswordEncrypted);
        // 明文密码
        String newPassword = RsaUtils.decryptByPrivateKey(newPasswordEncrypted);
        // 明文密码不匹配正则表达式
        if (!RegexUtils.isRegexMatch(oldPassword, RegexUtils.PASSWORD_REGEX)) {
            return KieooCommonResponse.builder().code(-1).message("旧"+RegexUtils.PASSWORD_REGEX_NOT_MATCH_MESSAGE).data(null).build();
        }
        // 明文密码不匹配正则表达式
        if (!RegexUtils.isRegexMatch(newPassword, RegexUtils.PASSWORD_REGEX)) {
            return KieooCommonResponse.builder().code(-1).message("新"+RegexUtils.PASSWORD_REGEX_NOT_MATCH_MESSAGE).data(null).build();
        }
        // 明文密码不匹配正则表达式
        if (oldPassword.equals(newPassword)) {
            return KieooCommonResponse.builder().code(-1).message("新密码不能与旧密码相同").data(null).build();
        }
        String userId = UserUtils.getUserId();
        UserBO userBO = new UserBO();
        userBO.setUserId(Long.parseLong(userId));
        UserPO userPO = userService.queryUserInfo(Long.parseLong(userId));
        userBO.setPassword(Sha512Utils.hash(userPO.getAccount() + newPassword));
        userService.updateUserPassword(userBO);
        return KieooCommonResponse.builder().code(0).message("user update success").data("").build();
    }

    /**
     * 更新用户信息
     *
     * @param userDTO userDTO
     */
    @PostMapping("/query/v1")
    public KieooCommonResponse queryUserInfo(@RequestBody UserDTO userDTO) {
        UserPO userPO = userService.queryUserInfo(Long.parseLong(userDTO.getUserId()));
        UserVO userVO = new UserVO();
        userVO.setUserId(userPO.getUserId());
        userVO.setNickName(userPO.getNickName());
        userVO.setHeadImg(userPO.getHeadImg());
        userVO.setSignature(userPO.getSignature());
        return KieooCommonResponse.builder().code(0).message("user query success").data(userVO).build();
    }
}
