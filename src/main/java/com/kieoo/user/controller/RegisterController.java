package com.kieoo.user.controller;

import com.kieoo.user.common.KieooCommonResponse;
import com.kieoo.user.entity.bo.UserBO;
import com.kieoo.user.entity.dto.UserDTO;
import com.kieoo.user.entity.vo.UserVO;
import com.kieoo.user.service.LoginService;
import com.kieoo.user.service.UserService;
import com.kieoo.user.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/register/")
public class RegisterController {
    @Autowired
    private UserService userService;
    @Autowired
    private LoginService loginService;

    /**
     * 使用账号进行注册
     *
     * @param userDTO userDTO
     */
    @PostMapping("/by-account/v1")
    public KieooCommonResponse byAccount(@RequestBody @Valid UserDTO userDTO) throws Exception {
        // 验证行为验证码
        CaptchaVerifyResult captchaVerifyResult = CaptchaUtils.verify(userDTO.getCaptchaVerification());
        // 行为二维码校验失败
        if (!captchaVerifyResult.isSuccess()) {
            // 返回状态码-1
            return KieooCommonResponse.builder().code(-1).message("captcha verify failed").data(null).build();
        }
        // 通过rsa私钥解密的方式获取明文密码
        String password = RsaUtils.decryptByPrivateKey(userDTO.getPasswordEncrypted());
        // 明文密码不匹配正则表达式
        if (!RegexUtils.isRegexMatch(password, RegexUtils.PASSWORD_REGEX)) {
            return KieooCommonResponse.builder().code(-1).message(RegexUtils.PASSWORD_REGEX_NOT_MATCH_MESSAGE).data(null).build();
        }
        // 获取账号
        String account = userDTO.getAccount();
        // 判断账号是否存在
        if (userService.accountExist(account)) {
            return KieooCommonResponse.builder().code(-1).message("该账号已被注册").data(null).build();
        }
        // 设置密码
        userDTO.setPassword(Sha512Utils.hash(account + password));
        // 添加用户
        userService.addUser(userDTO);
        // 用户登录
        UserBO userBO = loginService.queryUser(userDTO);
        UserVO userVO = new UserVO();
        userVO.setUserId(userBO.getUserId());
        userVO.setAccount(userBO.getAccount());
        userVO.setNickName(userBO.getNickName());
        userVO.setSex(userBO.getSex());
        userVO.setPhoneNumber(userBO.getPhoneNumber());
        userVO.setBirthDate(userBO.getBirthDate());
        userVO.setSignature(userBO.getSignature());
        userVO.setHeadImg(userBO.getHeadImg());
        userVO.setJwtToken(TokenUtils.sign(String.valueOf(userVO.getUserId()),"false"));
        return KieooCommonResponse.builder().code(0).message("register by account success").data(userVO).build();
    }

    /**
     * 通过手机号码注册
     *
     * @param userDTO userDTO
     */
    @PostMapping("/by-mobile/v1")
    public void byMobile(@RequestBody UserDTO userDTO) {
        // todo
    }

    /**
     * 判断账号是否存在
     *
     * @param account account
     * @return KieooCommonResponse
     */
    @GetMapping("/account-exist/v1")
    public KieooCommonResponse accountExist(@RequestParam("account") String account) {
        if (userService.accountExist(account)) {
            return KieooCommonResponse.builder().code(-1).message("account exist").data(null).build();
        }
        return KieooCommonResponse.builder().code(0).message("account not exist").data(null).build();
    }
}
