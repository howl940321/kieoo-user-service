package com.kieoo.user.controller;

import com.kieoo.user.common.KieooCommonResponse;
import com.kieoo.user.entity.dto.UserDTO;
import com.kieoo.user.service.HeadImgService;
import com.kieoo.user.utils.Base64Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Base64;

@Slf4j
@RestController
@RequestMapping("/headImg/")
public class HeadImgController {
    @Autowired
    private HeadImgService headImgService;

    @PostMapping("/update/v1")
    public KieooCommonResponse update(@RequestBody String headImgData) throws Exception{
        headImgService.update(headImgData.substring(24,headImgData.length()-1));
        return KieooCommonResponse.builder().code(0).message("update head img success").data(null).build();
    }
}
