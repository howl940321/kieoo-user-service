package com.kieoo.user.controller;

import com.google.common.base.Strings;
import com.kieoo.user.common.KieooCommonResponse;
import com.kieoo.user.entity.bo.UserBO;
import com.kieoo.user.entity.dto.UserDTO;
import com.kieoo.user.entity.vo.UserVO;
import com.kieoo.user.service.LoginService;
import com.kieoo.user.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author howl-xu
 * @version 1.0
 * Create by 2024/3/27
 */
@Slf4j
@RestController
@RequestMapping("/login/")
public class LoginController {

    private static Logger logger = LogManager.getLogger(LoginController.class);

    @Autowired
    private LoginService loginService;

    /**
     * 使用账号密码进行登录
     *
     * @param userDTO userDTO
     */
    @PostMapping("/by-account/v1")
    public KieooCommonResponse byAccount(@RequestBody UserDTO userDTO) throws Exception {
        // 行为验证码
        String captchaVerification = userDTO.getCaptchaVerification();
        // 验证行为验证码
        CaptchaVerifyResult captchaVerifyResult = CaptchaUtils.verify(captchaVerification);
        // 行为二维码校验失败
        if (!captchaVerifyResult.isSuccess()) {
            // 返回状态码-1,
            return KieooCommonResponse.builder().code(-1).message("验证码异常!").data(null).build();
        }
        // 加密后的密码，需要先解密
        String passwordEncrypted = userDTO.getPasswordEncrypted();
        // 明文密码
        String password = RsaUtils.decryptByPrivateKey(passwordEncrypted);
        // 获取账号
        String account = userDTO.getAccount();
        // 设置密码
        userDTO.setPassword(Sha512Utils.hash(account + password));
        // 查询用户信息
        UserBO userBO = loginService.queryUser(userDTO);
        // 用户信息不存在
        if (userBO == null) {
            // 返回状态码-1,
            return KieooCommonResponse.builder().code(-1).message("用户名或密码错误!").data(null).build();
        }
        // 封装用户信息后返回
        UserVO userVO = new UserVO();
        userVO.setUserId(userBO.getUserId());
        userVO.setAccount(userBO.getAccount());
        userVO.setNickName(userBO.getNickName());
        userVO.setSex(userBO.getSex());
        userVO.setPhoneNumber(userBO.getPhoneNumber());
        userVO.setBirthDate(userBO.getBirthDate());
        userVO.setSignature(userBO.getSignature());
        userVO.setHeadImg(userBO.getHeadImg());
        userVO.setJwtToken(TokenUtils.sign(String.valueOf(userVO.getUserId()),userDTO.getRememberMe()));
        return KieooCommonResponse.builder().code(0).message("login by account success").data(userVO).build();
    }

    /**
     * 通过手机号码登录，待实现
     *
     * @param userDTO userDTO
     */
    @PostMapping("/by-mobile/v1")
    public void byMobile(@RequestBody UserDTO userDTO) {
        //todo 空方法，后续会实现
    }
}
