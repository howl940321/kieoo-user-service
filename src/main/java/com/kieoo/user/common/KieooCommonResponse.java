package com.kieoo.user.common;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class KieooCommonResponse<T> {
    private int code;       // 状态码
    private String message; // 返回信息
    private T data;    // 响应数据
}
