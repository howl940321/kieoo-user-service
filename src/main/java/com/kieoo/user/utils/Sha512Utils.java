package com.kieoo.user.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Sha512Utils {

    /**
     * 测试哈希散列
     * @param args
     */
//    public static void main(String[] args) {
//        System.out.println(hash("测试哈希散列功能"));
//    }

    public static String hash(String message) {
        String sha512Hash = "";

        try {
            // 使用SHA512创建MessageDigest对象
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");

            byte[] messageBytes = message.getBytes();
            // 更新字节数组
            messageDigest.update(messageBytes);
            // 获取散列值的字节数组
            byte[] digest = messageDigest.digest();
            // 将字节数组转成十六进制字符串
            StringBuilder stringBuilder = new StringBuilder();

            for (byte b : digest) {
                stringBuilder.append(String.format("%02x", b));
            }

            sha512Hash = stringBuilder.toString();

        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

        return sha512Hash;
    }
}
