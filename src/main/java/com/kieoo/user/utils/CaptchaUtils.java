package com.kieoo.user.utils;

import com.alibaba.fastjson.JSONObject;
import okhttp3.*;
import com.alibaba.fastjson.JSON;
import java.io.IOException;

public class CaptchaUtils {

    public static CaptchaVerifyResult verify(String captchaVerification){
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
//        MediaType mediaType = MediaType.parse("text/plain");
        RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("captchaVerification",captchaVerification)
                .build();
        Request request = new Request.Builder()
                .url("http://127.0.0.1:7000/api/captcha/verify")
                .method("POST", body)
                .build();
        try {
            Response response = client.newCall(request).execute();
            String resStr = response.body().string();
            JSONObject jsonObject = JSON.parseObject(resStr);
            String repCode =  (String)jsonObject.get("repCode");
            String repMsg =  (String)jsonObject.get("repMsg");
            boolean success =  (boolean)jsonObject.get("success");
            CaptchaVerifyResult captchaVerifyResult = new CaptchaVerifyResult();
            captchaVerifyResult.setRepCode(repCode);
            captchaVerifyResult.setRepMsg(repMsg);
            captchaVerifyResult.setSuccess(success);
            return captchaVerifyResult;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

