package com.kieoo.user.utils;

import com.kieoo.user.entity.bo.UserBO;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class UserUtils {
    /**
     * 线程本地存储,用于在当前请求线程中快速获取userId
     */
    private static ThreadLocal<String> userInfo = new ThreadLocal<String>();


    public static String getUserId(){
        return userInfo.get();
    }

    public static void setUserId(String userId){
        userInfo.set(userId);
    }

    /**
     * shardingjdbc分表策略，这个方法在application.yml中通过反射有被使用，不要随意删除
     * @param bigDecimal
     * @return
     */
    public static int guessUserTable(BigDecimal bigDecimal){
        // 对传入的浮点数进行取整
        BigDecimal integerPart = bigDecimal.setScale(0, RoundingMode.DOWN);
        // 对取整结果求余后获取返回值
        return (int)(integerPart.remainder(new BigDecimal(2)).doubleValue());
    }
}
