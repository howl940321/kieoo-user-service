package com.kieoo.user.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtils {

    /**
     * 10到18位，需同时包含大写，小写，数字，特殊字符
     */
    public static final String PASSWORD_REGEX = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{10,18}$";
    /**
     * 正则表达式不匹配时返回给前端的message
     */
    public static final String PASSWORD_REGEX_NOT_MATCH_MESSAGE = "密码需包含大写，小写，数字，特殊字符，长度10到18位";

    /**
     *
     * @param text 文本内容
     * @param regex 正则表达式
     * @return boolean
     */
    public static boolean isRegexMatch(String text, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        return matcher.matches(); // 如果text满足regex，则返回true
    }
}
