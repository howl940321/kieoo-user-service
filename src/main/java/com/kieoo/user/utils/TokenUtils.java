package com.kieoo.user.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TokenUtils {
    //设置token过期时间
    private static final long DEFAULT_EXPIRE_TIME = 24 * 60 * 60 * 1000;
    //设置Token私钥
    private static final String TOKEN_SECRET = "thefirsttoken123";

    /**
     * 生成签名，15分钟过期
     *
     * @param **username**
     * @param **password**
     * @return
     */
    public static String sign(String userId, String remeberMe) {
        try {
            // 设置过期时间
            Date date = null;
            if ("false".equals(remeberMe)) {
                // 1天过期
                date = new Date(System.currentTimeMillis() + DEFAULT_EXPIRE_TIME);
            }
            if ("true".equals(remeberMe)) {
                // 30天过期
                date = new Date(System.currentTimeMillis() + 30 * DEFAULT_EXPIRE_TIME);
            }
            // 私钥和加密算法
            Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
            // 设置头部信息
            Map<String, Object> header = new HashMap<>(2);
            header.put("Type", "Jwt");
            header.put("alg", "HS256");
            // 返回token字符串
            return JWT.create()
                    .withHeader(header)
                    .withClaim("userId", userId)
                    .withExpiresAt(date)
                    .sign(algorithm);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * 检验token是否正确
     *
     * @param **token**
     * @return
     */
    public static boolean verify(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
            JWTVerifier verifier = JWT.require(algorithm).build();
            DecodedJWT jwt = verifier.verify(token);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    /**
     * 从token中获取登陆账户的uuid
     *
     * @param token
     * @return
     */
    public static String getLoginUserId(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim("userId").asString();
        } catch (JWTDecodeException e) {
            return null;
        }
    }
}
