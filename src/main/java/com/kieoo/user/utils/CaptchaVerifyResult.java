package com.kieoo.user.utils;

import lombok.Data;

@Data
public class CaptchaVerifyResult {
    // 状态码
    private String repCode;
    // 消息
    private String repMsg;
    // 是否成功标志位
    private boolean success;
}
