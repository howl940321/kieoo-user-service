package com.kieoo.user.mapper;

import com.kieoo.user.entity.po.UserPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * 订单Mapper接口
 *
 * @author howl-xu
 * @since 2024/3/22
 **/
@Mapper
@Component
public interface UserMapper {
    /**
     * 添加用户
     */
    int addUser(@Param("account") String account, @Param("password") String password, @Param("nickName") String nickName, @Param("sex") int sex, @Param("phoneNumber") String phoneNumber);

    int updateUser(@Param("userId") long userId,
                   @Param("nickName") String nickName,
                   @Param("signature") String signature,
                   @Param("birthDate") String birthDate);

    UserPO queryUserByAccountAndPassword(@Param("account") String account, @Param("password") String password);

    int updateUserPassword(@Param("userId") long userId, @Param("password") String password);

    UserPO queryUserInfo(@Param("userId") long userId);

    void updateUserHeadImg(@Param("userId") long userId, @Param("headImg") String headImg);
    int accountExist(@Param("account") String account);
}

