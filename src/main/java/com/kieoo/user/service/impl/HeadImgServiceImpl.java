package com.kieoo.user.service.impl;

import com.kieoo.user.mapper.UserMapper;
import com.kieoo.user.service.HeadImgService;
import com.kieoo.user.utils.Base64Utils;
import com.kieoo.user.utils.UserUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;

@Service
public class HeadImgServiceImpl implements HeadImgService {

    @Resource
    private UserMapper userMapper;
    @Override
    public void update(String base64Data) {
        // 获取用户id作为文件名
        String userId = UserUtils.getUserId();
        // 将文件保存到磁盘中
        Base64Utils.base64ToFile(base64Data, "/usr/share/nginx/html/static/head_img/" + userId + ".webp");
        // 图片上传到服务器后，还需要保存头像信息到数据库中
        userMapper.updateUserHeadImg(Long.parseLong(userId), userId+".webp");
    }
}
