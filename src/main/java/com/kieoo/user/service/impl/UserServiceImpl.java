package com.kieoo.user.service.impl;

import com.kieoo.user.entity.bo.UserBO;
import com.kieoo.user.entity.dto.UserDTO;
import com.kieoo.user.entity.po.UserPO;
import com.kieoo.user.entity.vo.UserVO;
import com.kieoo.user.mapper.UserMapper;
import com.kieoo.user.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author howl-xu
 * @since 2024-03-22
 */
@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;


    @Override
    public void addUser(UserDTO userDTO) {
        userMapper.addUser(userDTO.getAccount(), userDTO.getPassword(), userDTO.getNickName(), userDTO.getSex(), userDTO.getPhoneNumber());
    }

    @Override
    public void updateUser(UserBO userBO) {
        userMapper.updateUser(userBO.getUserId(), userBO.getNickName(), userBO.getSignature(), userBO.getBirthDate());
    }

    @Override
    public void updateUserPassword(UserBO userBO) {
        userMapper.updateUserPassword(userBO.getUserId(), userBO.getPassword());
    }

    @Override
    public UserPO queryUserInfo(long userId) {
        return userMapper.queryUserInfo(userId);
    }

    @Override
    public boolean accountExist(String account) {
        // 获取数据条数
        int count = userMapper.accountExist(account);
        // 1条代表存在,其他代表不存在
        return count == 1;
    }
}
