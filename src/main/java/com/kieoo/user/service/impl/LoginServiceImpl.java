package com.kieoo.user.service.impl;

import com.kieoo.user.entity.bo.UserBO;
import com.kieoo.user.entity.dto.UserDTO;
import com.kieoo.user.entity.po.UserPO;
import com.kieoo.user.mapper.UserMapper;
import com.kieoo.user.service.LoginService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class LoginServiceImpl implements LoginService {

    @Resource
    private UserMapper userMapper;
    @Override
    public UserBO queryUser(UserDTO userDTO) {
        UserPO userPO = userMapper.queryUserByAccountAndPassword(userDTO.getAccount(),userDTO.getPassword());
        if(userPO == null)
            return null;
        UserBO userBO = new UserBO();
        userBO.setUserId(userPO.getUserId());
        userBO.setAccount(userPO.getAccount());
        userBO.setNickName(userPO.getNickName());
        userBO.setSex(userPO.getSex());
        userBO.setPhoneNumber(userPO.getPhoneNumber());
        userBO.setBirthDate(userPO.getBirthDate());
        userBO.setSignature(userPO.getSignature());
        userBO.setHeadImg(userPO.getHeadImg());
        return userBO;
    }
}
