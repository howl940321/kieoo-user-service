package com.kieoo.user.service;

import com.kieoo.user.entity.bo.UserBO;
import com.kieoo.user.entity.dto.UserDTO;
import com.kieoo.user.entity.po.UserPO;
import com.kieoo.user.entity.vo.UserVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author howl-xu
 * @since 2024-03-22
 */
public interface UserService {

    void addUser(UserDTO userDTO);

    void updateUser(UserBO userBO);

    void updateUserPassword(UserBO userBO);
    UserPO queryUserInfo(long userId);
    // 判断账号是否存在
    boolean accountExist(String account);
}
