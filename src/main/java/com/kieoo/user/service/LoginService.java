package com.kieoo.user.service;

import com.kieoo.user.entity.bo.UserBO;
import com.kieoo.user.entity.dto.UserDTO;

public interface LoginService {
    UserBO queryUser(UserDTO userDTO);
}
