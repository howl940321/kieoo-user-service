package com.kieoo.user.service;

import com.kieoo.user.common.KieooCommonResponse;
import org.springframework.web.bind.annotation.RequestBody;

public interface HeadImgService {

    void update(String base64Data);
}
