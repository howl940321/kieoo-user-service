#!/bin/sh
#-----------------------------------------------------------------
# 进程的启动脚本,非项目启动必须脚本
# 这个脚本放在项目的jar包同级目录，直接执行脚本即可启动项目
# sh kieoo-user-service.sh start 启动项目
# sh kieoo-user-service.sh stop 停止项目
# sh kieoo-user-service.sh restart 重启项目
#-----------------------------------------------------------------

# 应用路径
SERVICE_DIR=/app/kieoo/user-service
# 应用名称
SERVICE_NAME=kieoo-user-service
# JAR包名称
JAR_NAME=$SERVICE_NAME\.jar
# 用于存储进程id的文件,停止脚本通过该文件获取进程id,然后执行kill指令结束进程
PID=process.pid

# 切换到应用路径
cd $SERVICE_DIR

case "$1" in

    start)
        ## 后台进程启动jar包,将非应用的控制台输出及报错重定向到/dev/null垃圾桶
        nohup java -Xms512m -Xmx512m -jar $JAR_NAME > /dev/null 2>&1 &
        echo $! > $SERVICE_DIR/$PID
        echo "=== 启动 $SERVICE_NAME"
        ;;

    stop)
        kill -9 `cat $SERVICE_DIR/$PID`
        rm -rf $SERVICE_DIR/$PID
        echo "=== 停止 $SERVICE_NAME"
        ;;

    restart)
		echo "=== 开始重启 $SERVICE_NAME"
        sh $0 stop
        sleep 2 
        sh $0 start
        echo "=== 重启完成 $SERVICE_NAME"
        ;;
        
    *)
        ## restart,不能匹配以上case的话，重启服务
		echo "=== 开始重启 $SERVICE_NAME"
        sh $0 stop
        sleep 10
        sh $0 start
        echo "=== 重启完成 $SERVICE_NAME"
        ;;
esac
exit 0
